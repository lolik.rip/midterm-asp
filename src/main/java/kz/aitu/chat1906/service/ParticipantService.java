package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Chat;
import kz.aitu.chat1906.model.Participant;
import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.repository.ChatRepository;
import kz.aitu.chat1906.repository.ParticipantRepository;
import kz.aitu.chat1906.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ParticipantService {
    private final ParticipantRepository participantRepository;
    private final UserRepository userRepository;
    private final ChatRepository chatRepository;

    public List<User> getUsersByChat(Long chatId) {
        List<Participant> participants = participantRepository.findParticipantsByChatId(chatId);
        return participants.stream().map(participant -> userRepository.findById(participant.getUserId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    public List<Chat> getChatsByUser(Long userId) {
        List<Participant> participants = participantRepository.findParticipantByUserId(userId);
        return participants.stream().map(participant -> chatRepository.findById(participant.getChatId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    public void add(Participant participant) {
        participantRepository.save(participant);
    }

    public void delete(Participant participant){
        participantRepository.delete(participant);
    }
}
